import logging
from datetime import datetime, timedelta, timezone
import requests
from celery import shared_task
from API.models import Products
from Dewu.settings import PARSE_PERIOD, API_URL


@shared_task()
def parse_spu_ids():
    products = []
    now = datetime.now(timezone.utc)
    for product in Products.objects.all():
        if product.spu_id.strip().isdigit() and now - product.create_time >= timedelta(hours=PARSE_PERIOD):
            products.append(product.spu_id.strip())
    print(products)
    try:
        url = f"{API_URL}parse/"
        requests.post(url, json=products)
    except Exception as e:
        print(e)


@shared_task()
def parse_spu(pk):
    product = Products.objects.get(pk=pk)
    spu_id = ""
    if 'https://dw4.co/t' in product.url:
        r = requests.get(product.url)
        spu_id = str(r.url)

    if 'spuId' in spu_id:
        spu_id = spu_id.split('spuId=')[-1].split('&')[0]
        if Products.objects.filter(spu_id=spu_id).exclude(pk=pk):
            product.delete()
        else:
            product.spu_id = int(spu_id)
            product.save()
            try:
                url = f"{API_URL}parse/"
                requests.post(url, json=[spu_id])
            except Exception as e:
                print(e)


@shared_task()
def parse_product(products):
    url = f"{API_URL}getOffers/"
    params = {
        "spuIds": products
    }
    result = requests.post(url, json=params)
    items = {}
    if result.ok:
        result = result.json()
        for item in result:
            items[item.get("spuId")] = item


    for spu_id in products:
        product = Products.objects.filter(spu_id=spu_id)
        if not product:
            continue
        product = product.first()
        item = items.get(f'{spu_id}')
        if not item:
            continue
        if not (product.override_title and product.override_title.strip()):
            product.override_title = item.get('title', '')
        if not (product.override_brand and product.override_brand.strip()):
            product.override_brand = item.get('params', {}).get('品牌', '')
        if not (product.sku and product.sku.strip()):
            product.sku = item.get('params', {}).get('主货号', '')
        product.save()


@shared_task()
def change_link():
    products = []
    for product in Products.objects.all():
        if not product.spu_id:
            if 'https://dw4.co/t' in product.url or 'spuId' in product.url:
                parse_spu.apply_async(args=[product.pk])
        if product.spu_id and not (product.override_title and product.override_brand and product.sku):
            products.append(product.spu_id)
    parse_product.apply_async(args=[products])
