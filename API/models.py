from django.db import models
from django.utils.translation import gettext as _
import requests
from django.utils.safestring import mark_safe


class TimeMixin(models.Model):
    create_time = models.DateTimeField(auto_now_add=True, verbose_name=_('Время создания'))

    class Meta:
        abstract = True


class Products(TimeMixin):
    url = models.CharField(verbose_name='URL', max_length=255, unique=True)
    spu_id = models.IntegerField(verbose_name='SPU', null=True, blank=True)
    override_title = models.CharField(verbose_name=_('Название (перезапись)'), max_length=255, null=True, blank=True)
    override_brand = models.CharField(verbose_name=_('Бренд (перезапись)'), max_length=255, null=True, blank=True)
    sku = models.CharField(verbose_name=_('SKU'), max_length=100, null=True, blank=True)
    comment = models.CharField(verbose_name=_('Comment'), max_length=255, null=True, blank=True)

    @property
    def thumbnail_preview(self):
        if self.spu_id:
            r = requests.get(f'https://cdn.dewucdn.com/spucdn-dewu/dewu/commodity/detail/v2/{self.spu_id}.json')
            if r.ok:
                src = r.json().get('data', {}).get('detail', {}).get('logoUrl', '')
                return mark_safe('<img src="{}" height="300" />'.format(src))
        return ""

    class Meta:
        verbose_name = _('Товар')
        verbose_name_plural = _('Товары')

    def __str__(self):
        return f'#{self.spu_id}'
