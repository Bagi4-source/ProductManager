from rest_framework import serializers
from . import models


class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Products
        fields = [
            'id',
            'url',
            'spu_id',
            'override_title',
            'override_brand',
            'sku',
            'comment',
            'create_time'
        ]
        read_only_fields = [
            'create_time'
        ]
