from import_export.resources import ModelResource
from django.contrib import admin
from .models import Products
from django import forms
from import_export.admin import ImportExportModelAdmin
from django.utils.translation import gettext as _


class ProductsAdminForm(forms.ModelForm):
    class Meta:
        model = Products
        fields = '__all__'


class ProductsResource(ModelResource):
    class Meta:
        model = Products
        skip_unchanged = True
        report_skipped = True
        import_id_fields = ("spu_id", "url")
        fields = ['url', 'spu_id', 'override_title', 'override_brand', 'sku', 'comment']


@admin.register(Products)
class ProductsAdmin(ImportExportModelAdmin):
    resource_classes = [ProductsResource]
    # form = ProductsAdminForm
    search_fields = ["spu_id", "override_title", "override_brand", "sku", "comment"]
    list_display = ("spu_id", "override_title", "override_brand", "sku", "comment", "create_time",)
    readonly_fields = ("create_time", "thumbnail_preview")
    date_hierarchy = "create_time"

    def thumbnail_preview(self, obj):
        return obj.thumbnail_preview

    thumbnail_preview.short_description = _('Thumbnail Preview')
    thumbnail_preview.allow_tags = True

    def get_image(self, a):
        return ''
